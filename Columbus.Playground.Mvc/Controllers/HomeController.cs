﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Columbus.Playground.Client.Abstractions;
using Columbus.Playground.Mvc.Models;

namespace Columbus.Playground.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly IXkcdClient _client;

        public HomeController(IXkcdClient client)
        {
            _client = client;
        }

        [Route]
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var carrier = await _client.GetComic();

            var model = new HomeModel
            {
                Title = carrier.Title,
                ImageUrl = carrier.Image
            };

            return View(model);
        }
    }
}