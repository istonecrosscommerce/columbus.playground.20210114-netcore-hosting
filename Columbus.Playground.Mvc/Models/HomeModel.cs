﻿namespace Columbus.Playground.Mvc.Models
{
    public class HomeModel
    {
        public string Title { get; set; }

        public string ImageUrl { get; set; }
    }
}