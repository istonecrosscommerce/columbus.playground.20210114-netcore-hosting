﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Columbus.Playground.Client.Abstractions;

namespace Columbus.Playground.Client
{
    public class XkcdClient : IXkcdClient
    {
        private const string XkcdClientBaseAddressKey = "XkcdClientBaseAddress";

        private readonly HttpClient _client;

        public XkcdClient()
        {
            var baseAddress = ConfigurationManager.AppSettings.Get(XkcdClientBaseAddressKey);

            _client = new HttpClient
            {
                BaseAddress = new Uri(baseAddress)
            };
        }

        public async Task<XkcdComicCarrier> GetComic()
        {
            var response = await _client.GetAsync("info.0.json");

            return await GetContent(response);
        }

        public async Task<XkcdComicCarrier> GetComicByNumber(int number)
        {
            var response = await _client.GetAsync($"{number}/info.0.json");

            return await GetContent(response);
        }

        private async Task<XkcdComicCarrier> GetContent(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsStreamAsync();

            return await JsonSerializer.DeserializeAsync<XkcdComicCarrier>(
                content,
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
        }
    }
}