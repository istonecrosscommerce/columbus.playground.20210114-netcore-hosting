﻿using System.Text.Json.Serialization;

namespace Columbus.Playground.Client.Abstractions
{
    public class XkcdComicCarrier
    {
        public string Alt { get; set; }

        public string Day { get; set; }

        [JsonPropertyName("img")]
        public string Image { get; set; }

        public string Link { get; set; }

        public string Month { get; set; }

        [JsonPropertyName("num")]
        public int Number { get; set; }

        [JsonPropertyName("safe_title")]
        public string SafeTitle { get; set; }

        public string Title { get; set; }

        public string Transcript { get; set; }

        public string Year { get; set; }
    }
}
