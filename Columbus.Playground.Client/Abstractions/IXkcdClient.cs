﻿using System.Threading.Tasks;

namespace Columbus.Playground.Client.Abstractions
{
    public interface IXkcdClient
    {
        Task<XkcdComicCarrier> GetComic();

        Task<XkcdComicCarrier> GetComicByNumber(int number);
    }
}